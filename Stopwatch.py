'''
Description: Stopwatch mini-project
Author: Thanh C. Tran
Ver: 1.0
Contact: thanh.tct1990@gmail.com
'''

# template for "Stopwatch: The Game"
import simplegui

# define global variables
count = 0

trials = 0
success = 0
running = False
tik = 0
print ("USAGE:")
print ("Click Start to run the clock!\n")
print ("Click Stop to halt the clock! and record number of successes per stop trials")
print ("Note: you successfully stop the clock if it stop at a whole second")
print ("Click Reset to reset the clock and success/trials number")

# define helper function format that converts time
# in tenths of seconds into formatted string A:BC.D
def format(t):
    ''' (int -> string) 
    Take the t tenths of a second 
    and returns a string of the form A:BC.D where A, C and D are digits 
    in the range 0-9 and B is in the range 0-5.
    >>> format(0)
    0:00.0
    >>> format(321)
    0:32.1
    >>> format(613)
    1:01.3
    '''
    global tik
    tik = t%10
    temp = t/10
    second = 0
    minute = 0
    if temp < 60:
        second = temp
    else:
        second = temp % 60
        minute = temp / 60
    if second < 10:
        return str(minute)+":0"+str(second)+"."+str(tik)
    else:
        return str(minute)+":"+str(second)+"."+str(tik)
    
# define event handlers for buttons; "Start", "Stop", "Reset"
def st():
    timer.start()
    global running
    running = True
        
def stp():
    timer.stop()
    
    global running
    global trials
    if running == True:
        trials = trials + 1
    global success
    if tik == 0 and running == True:
        success = success + 1
    
    running = False
def rst():
    timer.stop()
    global count
    count = 0
    global trials
    trials = 0
    global success
    success = 0
    global running
    running = False

# define event handler for timer with 0.1 sec interval
def counting():
    global count
    count = count + 1

# define draw handler
def draw_handler(canvas):
    canvas.draw_text(format(count), (40, 90), 50, 'White')
    canvas.draw_text(str(success)+"/"+str(trials), (160, 25), 25, 'Blue')
    
    
# create frame
frame = simplegui.create_frame('Stopwatch', 200, 150)
frame.set_draw_handler(draw_handler)

# register event handlers
timer = simplegui.create_timer(100, counting)

start = frame.add_button('Start', st)
stop = frame.add_button('Stop', stp)
reset = frame.add_button('Reset', rst)

# start frame
frame.start()

# Please remember to review the grading rubric
