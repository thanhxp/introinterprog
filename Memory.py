# implementation of card game - Memory

import simplegui
import random

# helper function to initialize globals
def new_game():
    global cardlist, exposed, state, turn, first, second
    state = 0
    turn = 0
    exposed = [False]*16
    cardlist = range(8)+range(8)
    random.shuffle(cardlist)
    print cardlist


     
# define event handlers
def mouseclick(pos):
    global exposed, state, turn, first, second
    index = pos[0]/50
    if state == 0:
        if exposed[index]==False:
            exposed[index]=True
            first = index
            print "first: ", first
            turn +=1
            state = 1
            print "state: ", state 
            print exposed
    elif state == 1:
        if exposed[index]==False:
            exposed[index] = True
            second = index
            print "second: ", second
            state = 2
            print "state: ", state  
            print exposed
    elif state == 2:
        if exposed[index] == False:
            if cardlist[first]<>cardlist[second]:
                exposed[first]=False
                exposed[second]=False
            exposed[index]= True
            first = index
            print "first: ", first
            turn +=1
            state = 1
            print "state: ", state
            print exposed
    
         
      
                        
# cards are logically 50x100 pixels in size    
def draw(canvas):
    for i in range(16):
        if exposed[i]==True:
            canvas.draw_text(str(cardlist[i]), [10+50*i, 75], 72, 'White')
        else:
            canvas.draw_polygon([(0+50*i, 0), (50+50*i, 0), (50+50*i, 100), (0+50*i, 100)], 1, 'Black', 'Green')
    label.set_text("Turns = " + str(turn))


# create frame and add a button and labels
frame = simplegui.create_frame("Memory", 800, 100)
frame.add_button("Restart", new_game)
label = frame.add_label("Turns = 0")

# register event handlers
frame.set_mouseclick_handler(mouseclick)
frame.set_draw_handler(draw)

# get things rolling
new_game()
frame.start()


# Always remember to review the grading rubric
