# Mini-project #6 - Blackjack

import simplegui
import random

# load card sprite - 949x392 - source: jfitz.com
CARD_SIZE = (73, 98)
CARD_CENTER = (36.5, 49)
card_images = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/cards.jfitz.png")

CARD_BACK_SIZE = (71, 96)
CARD_BACK_CENTER = (35.5, 48)
card_back = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/card_back.png")    

# initialize some useful global variables
in_play = False
outcome = ""
score = 0
result = ""

# define globals for cards
SUITS = ('C', 'S', 'H', 'D')
RANKS = ('A', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K')
VALUES = {'A':1, '2':2, '3':3, '4':4, '5':5, '6':6, '7':7, '8':8, '9':9, 'T':10, 'J':10, 'Q':10, 'K':10}


# define card class
class Card:
    def __init__(self, suit, rank):
        if (suit in SUITS) and (rank in RANKS):
            self.suit = suit
            self.rank = rank
        else:
            self.suit = None
            self.rank = None
            print "Invalid card: ", suit, rank

    def __str__(self):
        return self.suit + self.rank

    def get_suit(self):
        return self.suit

    def get_rank(self):
        return self.rank

    def draw(self, canvas, pos):
        card_loc = (CARD_CENTER[0] + CARD_SIZE[0] * RANKS.index(self.rank), 
                    CARD_CENTER[1] + CARD_SIZE[1] * SUITS.index(self.suit))
        canvas.draw_image(card_images, card_loc, CARD_SIZE, [pos[0] + CARD_CENTER[0], pos[1] + CARD_CENTER[1]], CARD_SIZE)
        
# define hand class
class Hand:
    def __init__(self):
        self.listcards = []

    def __str__(self):
        ans = "Hand contains "
        for i in range(len(self.listcards)):
            ans = ans + str(self.listcards[i]) + " "
        return ans

    def add_card(self, card):
        self.listcards.append(card)

    def get_value(self):
        # count aces as 1, if the hand has an ace, then add 10 to hand value if it doesn't bust
        hand_value = 0
        lackaces = True
        for i in range(len(self.listcards)):
            hand_value += VALUES[self.listcards[i].get_rank()]
            if self.listcards[i].get_rank() == "A":
                lackaces = False
        if lackaces:
            return hand_value
        else:
            if hand_value + 10 <= 21:
                return hand_value+10
            else:
                return hand_value

   
    def draw(self, canvas, pos):
        for i in range(len(self.listcards)):
            self.listcards[i].draw(canvas, [pos[0] + i*90, pos[1]])
 
        
# define deck class 
class Deck:
    def __init__(self):
        self.deckcards = []
        for suit in SUITS:
            for rank in RANKS:
                self.deckcards.append(Card(suit, rank))
                

    def shuffle(self):
        # shuffle the deck 
        random.shuffle(self.deckcards)

    def deal_card(self):
        # deal a card object from the deck
        return self.deckcards.pop()
    
    def __str__(self):
        # return a string representing the deck
        ans = "Deck contains "
        for i in range(len(self.deckcards)):
            ans = ans + str(self.deckcards[i]) + " "
        return ans



#define event handlers for buttons
def deal():
    global outcome, in_play, playerhand, dealerhand, deck, result, score
    
    if in_play == True:
        result = "You lose!"
        score -= 1
        in_play = False
        #print in_play
    else:
        outcome = "Hit or stand?"
        result = ""
        in_play = True
    
        playerhand = Hand()
        dealerhand = Hand()
        deck = Deck()
        deck.shuffle()
    
        playerhand.add_card(deck.deal_card())
        dealerhand.add_card(deck.deal_card())
        playerhand.add_card(deck.deal_card())
        dealerhand.add_card(deck.deal_card())
    
    #print "Player", playerhand
    #print "Player hand value:", playerhand.get_value()
    #print "Dealer", dealerhand
    #print "Dealer hand value:", dealerhand.get_value()

    # your code goes here
    

def hit():
    global outcome, in_play, playerhand, dealerhand, deck, result, score
    # replace with your code below
    # if the hand is in play, hit the player
    # if busted, assign a message to outcome, update in_play and score
    if playerhand.get_value() <= 21 and in_play == True:
        playerhand.add_card(deck.deal_card())
    if playerhand.get_value() > 21 and in_play == True:
        result = "You have busted and lose!"
        outcome = "New deal?"
        score -= 1
        in_play = False
    
    #print "Player", playerhand
    #print "Player hand value:", playerhand.get_value()
    #print "Dealer", dealerhand
    #print "Dealer hand value:", dealerhand.get_value()
    
def stand():
    global outcome, in_play, playerhand, dealerhand, deck, score, result
    # replace with your code below
    # if hand is in play, repeatedly hit dealer until his hand has value 17 or more
    # assign a message to outcome, update in_play and score
    if in_play == True:
        if playerhand.get_value() > 21:
            result = "You have busted"
            score -=1
        else:
            while dealerhand.get_value() < 17:
                dealerhand.add_card(deck.deal_card())
    
        if dealerhand.get_value() > 21:
            result = "Dealer has busted and you win!"
            score += 1
        else:
            if playerhand.get_value() <= dealerhand.get_value():
                result = "Dealer win!"
                score -= 1
            else:
                result = "Player win!"
                score += 1
    outcome = "New Deal?"
    in_play = False
            
            
# draw handler    
def draw(canvas):
    playerhand.draw(canvas, [50, 450])
    if in_play == True:
        canvas.draw_image(card_back, CARD_BACK_CENTER, CARD_BACK_SIZE, [50+CARD_BACK_CENTER[0], 200 + CARD_BACK_CENTER[1]], CARD_BACK_SIZE)
        dealerhand.listcards[1].draw(canvas, [140, 200])
    else:
        dealerhand.draw(canvas, [50, 200])
    canvas.draw_text("Blackjack Game", [20,60], 48, "White")
    canvas.draw_text("Dealer", [50,180], 32, "Black")
    canvas.draw_text("Player", [50,430], 32, "Black")
    canvas.draw_text(outcome, [200,430], 32, "Black")
    canvas.draw_text("Score: " + str(score), [400,100], 32, "Black")
    canvas.draw_text(result, [150,180], 32, "Black")


# initialization frame
frame = simplegui.create_frame("Blackjack Game", 600, 600)
frame.set_canvas_background("Green")

#create buttons and canvas callback
frame.add_button("Deal", deal, 200)
frame.add_button("Hit",  hit, 200)
frame.add_button("Stand", stand, 200)
frame.set_draw_handler(draw)


# get things rolling
deal()
frame.start()


# remember to review the gradic rubric
