'''
Description: Implementation of classic arcade game Pong
Author: Thanh C. Tran
Ver: 1.0
'''
import simplegui
import random

# initialize globals - pos and vel encode vertical info for paddles
WIDTH = 600
HEIGHT = 400
HALF_WIDTH = 300
HALF_HEIGHT = 200
BALL_RADIUS = 20
PAD_WIDTH = 8
PAD_HEIGHT = 80
HALF_PAD_WIDTH = PAD_WIDTH / 2
HALF_PAD_HEIGHT = PAD_HEIGHT / 2
LEFT = False
RIGHT = True
storedkey =[0,0,0,0]

# initialize ball_pos and ball_vel for new bal in middle of table
# if direction is RIGHT, the ball's velocity is upper right, else upper left
def spawn_ball(direction):
    global ball_pos, ball_vel # these are vectors stored as lists
    ball_pos = [HALF_WIDTH, HALF_HEIGHT]
    
    ball_vel = [0,0]
    ball_vel[0] = random.randrange(2, 4)
    ball_vel[1] = random.randrange(1, 3)
    
    if direction:
        ball_vel[1] = - ball_vel[1]
    else:
        ball_vel[0] = - ball_vel[0]
        ball_vel[1] = - ball_vel[1]
    
    print "ball velocity: ", ball_vel


# define event handlers
def new_game():
    global paddle1_pos, paddle2_pos, paddle1_vel, paddle2_vel  # these are numbers
    global score1, score2  # these are ints
    spawn_ball(random.choice([LEFT, RIGHT]))
    paddle1_pos = HALF_HEIGHT
    paddle2_pos = HALF_HEIGHT
    paddle1_vel = 0
    paddle2_vel = 0
    score1 = 0
    score2 = 0

def draw(c):
    global score1, score2, paddle1_pos, paddle2_pos, ball_pos, ball_vel
         
    # draw mid line and gutters
    
    c.draw_line([HALF_WIDTH, 0],[HALF_WIDTH, HEIGHT], 1, "White")
    c.draw_line([PAD_WIDTH, 0],[PAD_WIDTH, HEIGHT], 1, "White")
    c.draw_line([WIDTH - PAD_WIDTH, 0],[WIDTH - PAD_WIDTH, HEIGHT], 1, "White")
        
    # update ball
    ball_pos[0] += ball_vel[0]
    ball_pos[1] += ball_vel[1]
    
    # collide and reflect off the up and bottom of canvas
    if ball_pos[1] <= BALL_RADIUS or ball_pos[1] >= HEIGHT - 1 - BALL_RADIUS:
        ball_vel[1] = - ball_vel[1]
    elif (
          # Collide and reflect off the paddle
        (ball_pos[0] <= BALL_RADIUS + PAD_WIDTH 
        and 
        ball_pos[1] >= paddle1_pos - HALF_PAD_HEIGHT 
        and 
        ball_pos[1] <= paddle1_pos + HALF_PAD_HEIGHT)
        or 
        (ball_pos[0] >= WIDTH - 1 - BALL_RADIUS - PAD_WIDTH 
        and 
        ball_pos[1] >= paddle2_pos - HALF_PAD_HEIGHT 
        and 
        ball_pos[1] <= paddle2_pos + HALF_PAD_HEIGHT)
        ):
        ball_vel[0] = - 1.1*ball_vel[0]
        ball_vel[1] =   1.1*ball_vel[1]
        print "New ball velocity: ", ball_vel
    elif (
          # Collide and reflect off the left gutter
          ball_pos[0] <= BALL_RADIUS + PAD_WIDTH 
          and 
          (ball_pos[1] < paddle1_pos - HALF_PAD_HEIGHT 
           or 
           ball_pos[1] > paddle1_pos + HALF_PAD_HEIGHT)
         ):
        spawn_ball(RIGHT)
        score2 += 1
        print "Player2: ", score2
    elif (
          # Collide and reflect off the right gutter
          ball_pos[0] >= WIDTH - 1 - BALL_RADIUS - PAD_WIDTH 
          and 
          (ball_pos[1] < paddle2_pos - HALF_PAD_HEIGHT 
           or 
           ball_pos[1] > paddle2_pos + HALF_PAD_HEIGHT)
         ):
        spawn_ball(LEFT)
        score1 += 1
        print "Player1: ", score1
            
    # draw ball
    c.draw_circle(ball_pos, BALL_RADIUS, 2, "White", "White")
    #update paddle's velocity
    if storedkey[2] == 1:
        paddle1_vel = - 5
    elif storedkey[3] == 1:
        paddle1_vel =  5
    else:
        paddle1_vel = 0
        
    if storedkey[0] == 1:
        paddle2_vel = - 5
    elif storedkey[1] == 1:
        paddle2_vel =  5
    else:
        paddle2_vel = 0
    # update paddle's vertical position, keep paddle on the screen
    if paddle1_pos + paddle1_vel < HEIGHT - HALF_PAD_HEIGHT and paddle1_pos + paddle1_vel > HALF_PAD_HEIGHT:
        paddle1_pos += paddle1_vel
    if paddle2_pos + paddle2_vel < HEIGHT - HALF_PAD_HEIGHT and paddle2_pos + paddle2_vel > HALF_PAD_HEIGHT:
        paddle2_pos += paddle2_vel
    
    # draw paddles
    c.draw_polygon([[0, paddle1_pos - HALF_PAD_HEIGHT], 
                    [PAD_WIDTH, paddle1_pos - HALF_PAD_HEIGHT], 
                    [PAD_WIDTH, paddle1_pos + HALF_PAD_HEIGHT], 
                    [0, paddle1_pos + HALF_PAD_HEIGHT]],
                   1, 'White', 'White')
    #WIDTH-1 - PAD_WIDTH = 591
    #WIDTH-1 = 599
    c.draw_polygon([[591, paddle2_pos - HALF_PAD_HEIGHT], 
                    [599, paddle2_pos - HALF_PAD_HEIGHT], 
                    [599, paddle2_pos + HALF_PAD_HEIGHT], 
                    [591, paddle2_pos + HALF_PAD_HEIGHT]], 
                   1, 'White', 'White')
    
    # draw scores
    c.draw_text(str(score1), [125, 100], 100, 'White')
    c.draw_text(str(score2), [435, 100], 100, 'White')
        
def keydown(key):
    global paddle1_vel, paddle2_vel
    global storedkey
    if key==simplegui.KEY_MAP["up"]:
        storedkey[0] = 1
    elif key==simplegui.KEY_MAP["down"]:
        storedkey[1] = 1
    elif key==simplegui.KEY_MAP["w"]:
        storedkey[2] = 1
    elif key==simplegui.KEY_MAP["s"]:
        storedkey[3] = 1
    #print "Stored Key Array: ",storedkey
        
def keyup(key):
    global paddle1_vel, paddle2_vel
    global storedkey
    if key==simplegui.KEY_MAP["up"]:
        storedkey[0] = 0
    elif key==simplegui.KEY_MAP["down"]:
        storedkey[1] = 0
    elif key==simplegui.KEY_MAP["w"]:
        storedkey[2] = 0
    elif key==simplegui.KEY_MAP["s"]:
        storedkey[3] = 0
    #print "Stored Key Array: ",storedkey
    
def button_handler():
    new_game()

# create frame
frame = simplegui.create_frame("Pong", WIDTH, HEIGHT)
frame.set_draw_handler(draw)
frame.set_keydown_handler(keydown)
frame.set_keyup_handler(keyup)
button1 = frame.add_button('Restart', button_handler, 100)

# start frame
new_game()
frame.start()
