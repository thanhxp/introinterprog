# Introduction to Interactive Programming in Python #

### What is this repository for? ###
Source code of Game projects in [Introduction to Interactive Programming in Python](https://www.coursera.org/course/interactivepython1) offered by Coursera and Rice University.

### How do I get set up? ###
1. Copy and paste source code of each game into [link](http://www.codeskulptor.org/)
2. Click on the Run button in the top left conner
3. Experience the game!

### Contact ###
* [Cong Thanh Tran](mailto:mr.thanh.tct@gmail.com?Subject=[IntroInterPython]%20Questions)